import firebase from "firebase/app";
// If you are using v7 or any earlier version of the JS SDK, you should import firebase using namespace import

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";

var firebaseConfig = {
  apiKey: "AIzaSyAU9imm5qkOhyyRmF_bvsdzzzrVqVMpCTM",
  authDomain: "auth-dev-9a6c5.firebaseapp.com",
  projectId: "auth-dev-9a6c5",
  storageBucket: "auth-dev-9a6c5.appspot.com",
  messagingSenderId: "241553940679",
  appId: "1:241553940679:web:3bdc69b503510b1a6e1ee2"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAuth = firebaseApp.auth();
const firebaseDb = firebaseApp.database();


export { firebaseAuth, firebaseDb };



