import Vue from "vue";
import VueRouter from "vue-router";
import BackToTop from 'vue-backtotop'


import routes from "./routes";
import { firebaseAuth } from "../boot/firebase";

Vue.use(VueRouter);

Vue.use(BackToTop)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function(/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

//   Router.beforeEach((to, from, next) => {
// console.log('Router');
//   })
  //   // to and from are both route objects. must call `next`.
  //   const authRequired = to.matched.some(record => record.meta.authRequired);
  //   const requireGuest = to.matched.some(record => record.meta.requireGuest);
  //   if (authRequired) {
  //     if (!firebaseAuth.currentUser) {
  //       next({
  //         path: "/login"
  //       });
  //     } else {
  //       // console.log("Secret View", firebaseAuth.currentUser.email);
  //       next();
  //     }
  //   } else {
  //     next();
  //   }

  //   if (requireGuest) {
  //     if (firebaseAuth.currentUser) {
  //       console.log("Authenticated");
  //       console.log("Current User: ", firebaseAuth.currentUser);
  //       next();
  //     } else {
  //       console.log("Current User", firebaseAuth.currentUser);
  //       console.log("Not Authenticated");
  //       next();
  //     }
  //   } else {
  //     next();
  //   }
  // });

  return Router;
}
