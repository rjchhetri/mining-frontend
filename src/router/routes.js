import axios from "axios";
import { route } from "quasar/wrappers";
import { firebaseAuth } from "../boot/firebase";
import getters from "../store/store";

const APP_URL = "http://127.0.0.1:8000/api/";

// import axios from 'axios';


const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
   
   
    children: [
      {
        path: "/dashboard",
        component: () => import("src/pages/Dashboard.vue"),
        beforeEnter: (to, form, next) => {
          console.log('getters dash APP_URl'+getters);
          axios
            .get(APP_URL + "user")
            .then(response => {
              console.log("dashboard admin");
              if (response.data.roles == "admin") next();
              else {
                console.log("dashboard guest");
                next({
                  path: "/validate-lisence"
                });
              }
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      // {
      //   path: '/login', component: () => import('pages/Login.vue')

      // },
      {
        path: "/create-applicant",
        component: () => import("src/pages/CreateApplicant.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          // isLoggedIn()
          console.log("getters create APP_URl" + APP_URL);
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/update-renewal/:id",
        component: () => import("src/pages/UpdateRenewal.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          // const token = localStorage.getItem("token");

          // console.log("token is", token);

          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/generate-lisence",
        component: () => import("src/pages/RenewLisence.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/profile",
        component: () => import("src/pages/Profile.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/active-lisence",
        component: () => import("src/pages/ActiveLisence.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },

      {
        path: "/validate-lisence",
        component: () => import("src/pages/Validate.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              next();
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/expired-lisence",
        component: () => import("src/pages/ExpiredLisence.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },

      {
        path: "/renew-lisence/:id",
        component: () => import("src/pages/RenewLisence.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/dashboard",
        component: () => import("pages/Dashboard.vue"),
        // meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/applicants",
        component: () => import("pages/Applicants.vue"),
        meta: { authRequired: true },
        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/edit/:id",
        name: "UpdateApplicant",
        component: () => import("pages/UpdateApplicant.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/renew-lisencelist/:id",
        name: "AllRenewals",
        component: () => import("pages/AllRenewals.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/admin/",
        name: "Admin",
        component: () => import("pages/Admin.vue"),
        meta: { authRequired: true },

        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      },
      {
        path: "/chart/",
        name: "Chart",
        component: () => import("pages/Chart.vue"),
        beforeEnter: (to, form, next) => {
          axios
            .get(APP_URL + "user")
            .then(response => {
              if (response.data.roles == "admin") next();
              else
                next({
                  path: "/validate-lisence"
                });
            })
            .catch(err => {
              next({
                path: "/login"
              });
            });
        }
      }
    ]
  },
  {
    path: "/login",
    component: () => import("pages/Login.vue"),
    meta: { requireGuest: true },
    beforeEnter: (to, form, next) => {
      axios
        .get(APP_URL + "user")
        .then(response => {
          next("/dashboard");
        })
        .catch(err => {
          next();
        });
    }
  },

  // {
  //   path: "/login2",

  //   component: () => import("pages/Login2.vue"),
  //   beforeEnter: (to, from, next) => {
  //      if (getters.state.userDetails) {
  //       next({ name: "Posts" });
  //     } else {
  //       next();
  //     }
  //   }
  // },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

// beforeEnter: (to, form, next) => {
//   axios
//     .get(APP_URL+"athenticated")
//     .then(() => {
//       next();
//     })
//     .catch(() => {
//       return next({
//         path: "/login"
//       });
//     });
// };

// routes.router.beforeEach((to, from, next) => {
//   // to and from are both route objects. must call `next`.
//   const authRequired = to.matched.some(record => record.meta.authRequired);
//   const requireGuest = to.matched.some(record => record.meta.requireGuest);
//   if (authRequired) {
//     if (!firebaseAuth.currentUser) {
//       next({
//         path: "/login"
//       });
//     } else {
//       console.log("Secret View", firebaseAuth.currentUser.email);
//     }
//   } else {
//     next();
//   }

//   if (requireGuest) {
//     if (firebaseAuth.currentUser) {
//       console.log("Authenticated");
//       console.log("Current User: ", firebaseAuth.currentUser);
//       next({
//         path: "/secret"
//       });
//     } else {
//       console.log("Current User", firebaseAuth.currentUser);
//       console.log("Not Authenticated");
//       next();
//     }
//   } else {
//     next();
//   }
// });

export default routes;
